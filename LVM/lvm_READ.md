# Краткая статья о работе с LVM

Для начала крайне **НЕ** рекомендую уменьшать размер диска. ибо если нормально маны не прочитаешь то всё будет **очень плохо**.

Допустим существует задача перенести `/home`  с физики на другой раздел куда в lvm что бы в дальнейшем было удобно работать. 

Первым делом обозначу картину с помощью `lsblk`

```sh
NAME             MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0               11:0    1 69.1M  0 rom  
xvda             202:0    0    9G  0 disk 
`-xvda1          202:1    0    9G  0 part 
  |-deb--vg-boot 254:0    0  488M  0 lvm  /boot
  |-deb--vg-root 254:1    0  6.5G  0 lvm  /
  `-deb--vg-home 254:2    0  1.5G  0 lvm  /home
xvdb             202:16   0    8G  0 disk 
```

Как видно у нас имеется диск с 8 гигами памяти который мы будем пилить на разделы и эксперементировать.

И так начнём:
 
 1. Отдадим пространство под PV
```sh
 pvcreate /dev/xvdb
```
 2. Проверим с помощью **pvscan** и результат будет таким
```sh
  PV /dev/xvda1   VG deb-vg          lvm2 [9.00 GiB / 512.00 MiB free]
  PV /dev/xvdb    VG deb-vg          lvm2 [8.00 GiB / 8.00 GiB free]
  Total: 2 [16.99 GiB] / in use: 2 [16.99 GiB] / in no VG: 0 [0   ]
```
 3. Теперь создадим VG для размещения в нём логических дисков 
    Первый аргумет это навзание группы, второе - её расположение.
 ```sh   
  vgcreate deb-vg /dev/xvdb 
 ```
 4. А теперь приступим к созданию самого логического раздела.

```sh
 lvcreate -L 1G -n backup deb-vg
```
    -L - указание размера в понятном нам формате (G - gigabyte M - megabyte etc...)
    
    -l - указание размера в блоках LVM (PE). Смотреть в **vgdisplay**.
    
    -n - имя создаваемого раздела.

    и в последнюю очередь указывается имя VG где будет размещён логический раздел.

 5. Проверим успешность создания с командой **lvdisplay**
 ```sh
 --- Logical volume ---
  LV Path                /dev/deb-vg/backup
  LV Name                backup
  VG Name                deb-vg
  LV UUID                5jdXnf-i0cw-fbSs-AE33-DZkJ-2hgO-szKOmG
  LV Write Access        read/write
  LV Creation host, time polygon, 2021-06-09 16:26:12 +0300
  LV Status              available
  # open                 1
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:3
 ```

 6. Ну и дальше работать с разделом как с обычным устройством, по этому создадим файловую систему и примонтируем, в качестве примера как `/backup`.
```sh
 mkfs.ext4 /dev/deb-vg/backup
```
Небольшое замечание, ниже мы монтируем (привязываем) содержимое диска к папке, которую создаём 

```sh
 mkdir /mnt/backup  # моздаём папку и монтируем к ней логический раздел

 mount /dev/deb-vg/backup /mnt/backup
```

 7. При необходимости иметь постоянную привязку раздела `backup` к папке `/mnt/backup`  добавим в `/etc/fstab` строчку 
 ```sh
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/mapper/deb--vg-root /               ext4    errors=remount-ro 0       1
/dev/mapper/deb--vg-boot /boot           ext2    defaults        0       2
/dev/mapper/deb--vg-home /home           ext4    defaults        0       2    # вот она наша строчка с полным указанием пути для монтирования
/dev/mapper/deb--vg-backup /mnt/backup  ext4     defaults        0       2 
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
 ```

 8. Далее необходимо провериь правильность описанной команды, для этого используем `mount -a` ну и лишним не будет - `df -h` и если всё подключится - можно идти на перезагрузку.
 ля примера укажу в файле несуществующую папку и получу вот такое сообщение об ошибке:
 ```sh
 mount: mount point /mnt/backu does not exist
 ```
