# Nginx краткая информация

## Используемые команды

`nginx -t` - проверка конфигурации, полезно перед окончательным рестартом

## Описание конфигурации

Nginx позволяет настраивать виртуальные хосты на одном физическом сервере (не путать с прокси сервером!).

`/etc/nginx/sites-available/` - доступные конфиги для сайтов
`/etc/nginx/sites-enabled/` -  конфиги фалов которые в данный момент запущены  

для запуска сайта необходимо добавить символическиу ссылку из папки `sites-available` в `sites-enabled`.

```sh
ln -s /etc/nginx/sites-available/<your_site> /etc/nginx/sites-enabled/  # создание ссылки
sudo service nginx restart 
```

Что бы рзместить несколько сайтов на одном  IP то создаём два конфигурационных файла (лучше с именами сайтов например "kufar.by" "kufar.com"), и в них указываем `server_name` для разграничение трафика каждому из доменов. Ну и желательно сделать редирект с http на https.

Также при использовании `php-fpm` олезно было бы (изучить вот это)[https://interface31.ru/tech_it/2016/04/nginx-php-fpm-debian-ubuntu.html]

### пример конфигурции сайтов

 Два ниже идущих примера это работа двух сайтов на одном хосте (одном IP)

```bash
server {
    listen 80;
    server_name www.test.commontoools.net test.commontools.net;
    return 301 https://test.commontools.net;
}

server {
    
    # переключаем сайт на https 
   listen 443 ssl;
   server_name         www.test.commontools.net  test.commontools.net;
   ssl_certificate     /etc/ssl/wildcard.commontools.net.chained.crt;
   ssl_certificate_key /etc/ssl/private/wildcard.commontools.net.key;

    # указываем корневой каталог сайта
   root /usr/share/nginx/html;
   index index.html index.htm;
    
    # логи
   access_log /var/log/nginx/test.commontools.log;  
   error_log /var/log/nginx/test.commontools_error.log;

   location / {
       try_files $uri $uri/ =404;
   }
}
```

А если используем утилиту `certbot` совместно с центром сертификиции `Let's Encrypt` то при автоматической конфигурации можем видеть что то наподобии этого:

```sh
server {
   server_name www.falcongaze.by falcongaze.by;

   root /var/www/falcongaze;
   index index.html;

   access_log /var/log/nginx/falcongaze_by.log;
   error_log /var/log/nginx/falcongaze_error.log;

   location / {
       try_files $uri $uri/ =404;
   }


    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/falcongaze.by/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/falcongaze.by/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = www.falcongaze.by) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = falcongaze.by) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

   listen 80;
   server_name www.falcongaze.by falcongaze.by;
    return 404; # managed by Certbot
}
```
