# Fail2ban

Компонентная утилита для защиты от перебора пароля для рута и иных пользователей, а так же может работать и для защиты веб версий (но это нужно проверить)
Принцип работы заключается в добавлении ip адресов в собственный `chain` в `iptables`

Инсталируем из репозитория - `sudo apt install fail2ban`

Имеются два основных файла конфигурации:
`/etc/fail2ban/fail2ban.conf` — отвечает за настройки запуска процесса Fail2ban.
`/etc/fail2ban/jail.conf` — содержит настройки защиты конкретных сервисов, в том числе sshd.

Файл jail.conf поделён на секции ( изоляторы - "jails"), каждая секция отвечает за определённый сервис и тип атаки:

```sh
[DEFAULT]  # пример jail для  дефолтной конфигурации
ignoreip = 57.66.158.131



[ssh]  # пример для сервиса ssh
enabled = true
port = ssh
filter = sshd
action = iptables[name=sshd, port=ssh, protocol=tcp] sendmail-whois[name=ssh, dest=****@yandex.ru, sender=fail2ban@***.ru]
logpath = /var/log/auth.log
maxretry = 3
bantime = 600
```
