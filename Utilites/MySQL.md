# MySQL 5.7 or higher some features

## Comands

**`;`** - It is important thing. You **must** USE it!

### Путешествие по бд

`SHOW databases;` - отобразить все базы данных.

`USE [database];` -  использовать выбранную бд.

`SHOW GRANTS;` or `SHOW GRANTS FOR 'test-user';` - отобразить все права текущего пользователя.

`SHOW TABLES;` - отобразить таблицы в бд.

`SHOW COLLOCATION;` - тобразить поддерживаемые кодировки.

`SHOW processlist;` - отобразить сессии пользователей.

`DESCRIBE [table_name] ;` - тобразить поля таблицы с их типом данных.

`SELECT * from [table_name] ;` - вывести все поля с значениями из таблицы.  

### Созидание

Создание БД и таблиц

`CREATE database [name_db] ;` - создание БД.

`CREATE TABLE table_name ( [field_1] [field_type]([number]), [field_1] [field_type]([number]));` - создание таблицы с полями.

`INSERT INTO table_name (col1, col2, col3, ...) VALUES` - вставка записи с указанными значеними

Создание пользователей и назначение прав

`CREATE USER '[user_name]'@'[type_connection]' IDENTIFIED BY '[user_password]';` - создание пользователя

`GRANT [List_of_rules] ON [database_name].[table_name] TO 'user_name'@'type_connection' IDENTIFIED BY 'password';` - назначение привелегий пользователю

`REVOKE [privileges] ON [database_name].[table_name] FROM '[user]'@'[host]';` - отзыв привелегий у пользователя

### Разрушение

`DROP DATABASE [name];` - роняем базу.

`DROP TABLE [name];` - роняем таблицу.

`DROP USER 'guest'@'localhost';` - роняем пользователя

### БЭКАПИРОВАНИЕ

```bash
# mysqldump -u[пользователь] -p[пароль_пользователя] [имя_базы] > [название_файла_резервной_копии_базы].sql

mysqldump -uroot -p12345 --single-transactions telegramDB > /mnt/backup/mysql_dump_20-09-21.sql

# "--single-transactions" - создает дамп в виде одной транзакции

```

### Some features

`FLUSH PRIVILEGES;` - перечитать права пользователей.

`` ---

``

### Прмеры

```sql
SELECT User, Host, Password, plugin from user;  -- dsf
SELECT * from Employers where speciality like '%DevOps%'; -- поск работников со строчкой DevOps
CREATE table gentoo (user Varchar(128), email Varchar(128)) --
DROP database if exists `long-strange?-nameDB` /*  */

CREATE user 'super-admin'@'localhost' identified by 'password';

GRANT SELECT, CREATE, insert, delete, update, DROP ON superdb.* TO 'super-admin'@'localhost' identified by 'password';

```  

## Troubleshuting

---

### ibtmp1 - файл слишком большой (увеличился за короткий срок)

---
Во-первых стоит ознакомится вот с [этой ссылкой](https://dev.mysql.com/doc/refman/5.7/en/innodb-temporary-tablespace.html)
А потом можно и вот [с этой](https://blog.programs74.ru/temporary-tablespace-in-mysql/)

Если кратко, то как правило MySQL использует временное табличное пространство для хранения временных данных операций сортировки при выполнении запросов пользователей или при создании индексов и т.п.

При нормальном завершении работы MySQL файл временного ТП удаляется, а при запуске создается новый файл с новым идентификатором. (т.е. при `service mysql restart`).

Если в дальнейшем проблема повторится, то можно ограничить размер данного временного файла, хотя это и не избавит от необходимости чистить его переодически, ибо при "тяжёлых" запросах они просто напрсото будут завешаться с ошибкой.
тем не менее в файле `/etc/mysql/mysql.conf.d/mysqld.cnf` укажем:

```sh
[mysqld] #эта секция уже должна существовать
innodb_temp_data_file_path=ibtmp1:12M:autoextend:max:10G   #а вот эту строчку дописываем 
# где G - gigabyte, M - megabyte, K- kilobyte.
```

### Сброс пароля root

---
Всё в том же файле `/etc/mysql/mysql.conf.d/mysqld.cnf` отключаем проверку таблиц (предварительно залогинившись в систему под рутом).
Дописываем строчку и перезапускаем сервис.

```sh
[mysqld]
skip-grant-tables # обственно отключение проверяющих таблиц
```

Можно так же и с помощью добавления строчки `mysqld --skip-grant-tables` но не всегда он отрабаывает.

После манипуляции логинимся, сбрасываем пароль, и в конце убираем добавленную строчку из конфигурационного файла.
