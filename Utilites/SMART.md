# SMART in linux with smartctl

### Install
1. для установки ввести комманду `sudo apt install smartmontools` 
2. оскольку утилита обращается к HDD или SSD на низком уровне то нужны привелигированные права
3. для включения SMART на диске используй команду `smartctl -s on`

### Use

`smartctl --scan`  - показывает доступные устройства.

`smartctl -i /dev/<your_device>` - показывает информацию о диске

`sudo smartctl -t <short|long|conveyance|select> /dev/<your_device>` - тестирование накопителя с предложенным вариантом. не влияет на работу накопителя, так что можно делать на боевом + во время работы сервиса?.

`smartctl -a /dev/<your_device>` - показывает состояние диска после тестирования.
Если они будут то вывод всё покажет.

`smartctl -c /dev/<your_device>` -  показывает сколько вермени займёт каждый тест.

`smartctl -H /dev/<your_device>` -  быстро показывает здоровье HDD

### For RAID arrays

В отличии от обычных дисков RAID-массивы определяются и отображаются по-разному, и для начала нужно всех найти : `smartctl --scan`

Вывод приблизительно будет таким:
```sh
/dev/sda -d scsi # /dev/sda, SCSI device
/dev/sdb -d scsi # /dev/sdb, SCSI device
/dev/sdc -d scsi # /dev/sdc, SCSI device
/dev/bus/0 -d megaraid,0 # /dev/bus/0 [megaraid_disk_00], SCSI device
/dev/bus/0 -d megaraid,1 # /dev/bus/0 [megaraid_disk_01], SCSI device
/dev/bus/0 -d megaraid,2 # /dev/bus/0 [megaraid_disk_02], SCSI device
/dev/bus/0 -d megaraid,3 # /dev/bus/0 [megaraid_disk_03], SCSI device
```
Поскольку я заранее знаю (`lsblk`) что `/dev/sda/` это и есть RAID10 массив состоящий и 4 дисков  то для просмотра каждого отдельного диска необходимо выполнить:

`smartctl -a -d megaraid,<your_number> /dev/sda` - используется при проверке диска, если он находится в **RAID!!!** (опция -d с параметром может отличатся от рейда к рейду)

Для проверки же целостности состояния диска, нужно запустить тестирования.

`smartctl -t long -d megaraid,3 /dev/sda` - долгая и полная проверка дисков, не влияет на работу.



