# IPTABLES

Лучше всего описано функционирование данного сетевого фильтра [здесь](https://www.opennet.ru/docs/RUS/iptables/)

В основном мы будем работаь с таблицей `filter` поскольку (и судя по назвнию) в ней осуществляется фильтрация трафика.
Таблицы `nat` и `managle` будут рассмотрены при необходимости.

У таблицы `filter` есть три цепочки :

`INPUT` - пакеты преднзаначенные системе.

`FORWARD` - пакеты предназначенные для другой системы.

`OUTPUT` - пакеты исходящие из текущей системы.

Краткие и используемые выдержки:

Вывод текущих настроек таблицы `filter`

```sh
 iptables -L -n -v --line-numbers # отобразить список правил с доп информациией и по порядку
 # где  n -  отображает IP-адреса и номера портов в числовом виде без преобразования их в символические имена (используется своместно с --list -L)
 #      v -  выводит дополнительную информацию: счетчики пакетов и байт для каждого правила
 #      line-numbers - выводит порядковый номер каждого правила 
 #  все выше опсанные команды зачастую используются с командой вывода (-L  or --list)

 iptables -S # отображает правила в прописанном состоянии, а не в результативном
```

Добавление рарешения на пропуск `https` трафика извне и от сервера.

```sh
где `-m` (`--match`) это ызов дополнительного критерия в данном случае с парамертрами

 iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

 iptables -A OUTPUT -p tcp --sport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT
```

Добавленеи и удаление правил по порядковому номеру.

```sh
 sudo iptables -D INPUT <number> # удалить строчку с номером для цепочки INPUT таблицы filter 

 sudo iptables -I INPUT <number> -s 5.100.193.142  -j DROP  # вставить правило на указанную позицию в цепочку INPUT таблицы filter

 sudo iptables -I INPUT <NUMBER> -s 95.47.60.89/32 -j ACCEPT # Толян
```

