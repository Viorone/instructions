# Certbot - для управления сертификатами

Утилита позволяющая выпускать сертификаты подтверждённые Let's Encrypt.

## Основные команды

```sh
certbot revoke --cert-path /etc/letsencrypt/live/CERTNAME/cert.pem  # удаление сертификата с серверов Let's Encrypt --проверить

certbot delete --cert-name example_site.com # удаляет сертификаты и все симлинки на локальном сервере --проверить

certbot -renew --cert-name <cert-name> --dry-run  # запуск тестового обновления сертификата 
```

## Описание и использование

---

### Установка и выпуск сертификата

Установка и использование с `nginx`:

`sudo apt install certbot python3-certbot-nginx` - устанавливаем с доп модулем.

Создаём файл с именем сайта для которого создаём сертификат

`sudo nano /etc/nginx/sites-available/<you_site>`

и прописываем в этом файле имя (вообще настоятельно рекомендую заранее прописать конфигурацию хотя бы на http соединение, ибо в неё будет `certbot` дописывать свой конфиг)

`server_name <you_site> www.<you_site>;`

Далее проверяем конфигурацию nginx :

`sudo nginx -t`

И если всё гуд перезапускаем сам nginx.

Генерируем сертфикат:

`sudo certbot --nginx -d <you_site> -d www.<you_site>`

Далее будет предложено ввести почтовый ящик - вводим -> далее не соглашаемся с последующим пунктом -> далее выставляем полный редирект.
Тперь сайт на `https` оживёт.

### Перевыпуск сертификата

Обычно при использовании `certbot` он добавляет задачу в папку `/etc/cron.d/certbot` и следит за автоматичским продлением. тем не менее можно и попробовать обновить вручную.

`certbot -renew --cert-name <cert-name> --dry-run`

Здесь:

- `--cert-name` - это имя сертификата (не путать с доменным именем, хотя они и могут быть похожи) которое было дано при первом выпуске, что бы посмотреть его достаточно выполнить команду `sudo  certbot certificates` и примерный вывод такой:

    ```sh
    Saving debug log to /var/log/letsencrypt/letsencrypt.log

    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Found the following certs:
      Certificate Name: profi.by            # имя сертификата
        Domains: profi.by www.profi.by      # доменное имя
        Expiry Date: 2021-12-14 08:42:53+00:00 (VALID: 89 days)
        Certificate Path: /etc/letsencrypt/live/prof.by/fullchain.pem
        Private Key Path: /etc/letsencrypt/live/prof.by/privkey.pem
      Certificate Name: folcan.by
        Domains: folcan.by www.folcan.by
        Expiry Date: 2021-11-22 08:57:15+00:00 (VALID: 67 days)
        Certificate Path: /etc/letsencrypt/live/falcongaze.by/fullchain.pem
        Private Key Path: /etc/letsencrypt/live/falcongaze.by/privkey.pem
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    ```

- `--dry-run` -  тестовый перевыпуск сертефиката, вообще данная команда крайне рекомендательна при ручном запуске перевыпуска сертификатов и других важных команд.

По итогу рабочий пример выглядит так: `certbot -renew --cert-name profi.by`.
