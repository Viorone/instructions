
# Краткие заметки по использованию Vagrant

Так же для начала необходимо обновить в виртуалбоксе исо образ который отвечает за гостевой проброс папок в вагрант

При возникающей обибке интегрирования модуля Vbox для гостевых систем нужно установить `linux-headers`

Пример:

```sh
sudo apt-get install linux-headers-$(uname -r)
```

 или
vag 
```sh
sudo apt-get install build-essential linux-headers-`uname -r` dkms
```

Если ошибка не пропадает то можно создать вручную ссылку на ваш гостевой агент бокса, однако срабоает только если к ВМ подключён образ `VBoxGuestAdditions`. 

проверка: `ls -lh /sbin/mount.vboxsf` 

```sh
sudo ln -sf /opt/VBoxGuestAdditions-*/lib/VBoxGuestAdditions/mount.vboxsf /sbin/mount.vboxsf
```

Так же при использваонии некторых плейбуков необходим наличие `acl` для выполнения команд от другого пользователя

```sh
Failed to set permissions on the temporary files Ansible needs to create when becoming an unprivileged user (rc: 1, err: chown: changing ownership of '/var/tmp/ansible-tmp-1645442969.756278-62081502773255/': Operation not permitted\
```


Образ оси для эмуляции мака `vagrant init jhcook/macos-sierra`