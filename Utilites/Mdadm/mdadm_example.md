# mdadm – утилита для работы с программными RAID-массивами в Linux. (Softaware RAID)

Сразу приступим к практике:

Очищаем сектор суперблока у двух дисков
`[root@by05 ~]# mdadm --zero-superblock --force /dev/sd{b,c}` 

Если вывод как ниже то, блоков до этого не существовало, и они были созданы.
mdadm: Unrecognised md component device - /dev/sdb
mdadm: Unrecognised md component device - /dev/sdc



[root@by05 ~]# mdadm --create --verbose /dev/md0 -l 1 -n 2 /dev/sd{b,c}



mdadm: partition table exists on /dev/sdb
mdadm: partition table exists on /dev/sdb but will be lost or
       meaningless after creating array
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
mdadm: partition table exists on /dev/sdc
mdadm: partition table exists on /dev/sdc but will be lost or
       meaningless after creating array
mdadm: size set to 1953382464K
mdadm: automatically enabling write-intent bitmap on large array

Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md0 started.


[root@by05 ~]# mkfs.ext4 /dev/md0 
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
122093568 inodes, 488345616 blocks
24417280 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2636120064
14904 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
	102400000, 214990848

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done       

