# Гайд по расширению дисков LVM

!!! ПЕРЕПИСАТЬ НОРМАЛЬНО !!!

## Вводная информация
Для начала нужно увеличить обьём диска на виртуалке и определится что у нас есть в системе: `lsblk`

Мы будем расширять `xvda5` методом `parted` и `xvdc1` методом `disk`

Вывод: 
```sh
NAME                            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0                              11:0    1 1024M  0 rom  
xvda                            202:0    0   10G  0 disk      # видно что доступно 10 gb  
|-xvda1                         202:1    0  243M  0 part /boot
|-xvda2                         202:2    0    1K  0 part 
`-xvda5                         202:5    0  7.8G  0 part      # вот сюда и добавим из свободного места
  |-debian--vg-root             254:1    0    8G  0 lvm  /
  `-debian--vg-swap_1           254:2    0  728M  0 lvm  [SWAP]
xvdb                            202:16   0    8G  0 disk 
`-debian--vg--home-vg----LVhome 254:0    0    4G  0 lvm  /home
xvdc                            202:32   0    4G  0 disk        #видим что сам диск видит 4 ГБ
`-xvdc1                         202:33   0    3G  0 part        # при этом раздел, на котором держаться LVM имеет 3 ГБ
  |-debian--vg-root             254:1    0    8G  0 lvm  /
  `-debian--vg-backup           254:3    0    2G  0 lvm  
```
Ну и далеее получим информацию о состоянии **PV** **VG** **LV**

`pvs`:
```sh
root@debian:~# pvs
  PV         VG             Fmt  Attr PSize PFree
  /dev/xvda5 debian-vg      lvm2 a--  7.76g    0 
  /dev/xvdb  debian-vg-home lvm2 a--  8.00g 4.00g   #в данном примере свободное место для группы debian-vg-home не рассматриваем
  /dev/xvdc1 debian-vg      lvm2 a--  3.00g    0                                        # ибо  работаем с группой debian-vg
```

`vgs`:
```sh
root@debian:~# vgs
  VG             #PV #LV #SN Attr   VSize  VFree
  debian-vg        2   3   0 wz--n- 10.75g    0 
  debian-vg-home   1   1   0 wz--n-  8.00g 4.00g
```

`lvs`:
```sh
root@debian:~# lvs
  LV         VG             Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup     debian-vg      -wi-a-----   2.03g                                                    
  root       debian-vg      -wi-ao----   8.01g                                                    
  swap_1     debian-vg      -wi-ao---- 728.00m                                                    
  vg--LVhome debian-vg-home -wi-ao----   4.00g   
```
Как видно из вышестоящего вывода определяем что место появилось на `xvdc` под которым уже существует 2 ЛВМ (`backup` и расширение для `root`), ну и поскольку место не распределено (можно судить по `PV` и `VG` ибо значения `PFree` и `VFree` равны нулю) то можно поробовать расширить.

## Начинаем расширять


Прежде чем начать расширять хотел бы рассказать о   возможных вариантах :
  


1. Диск не содержит разделов, и сразу отдан под PV. 

    Наверное один из самых приятных вариантов, ибо при расширении диска, достаточно будет использовать `pvresize` для увеличения диска без риска танцев с бубном. На отображаемой информации `lsblk` и `pvs` можно как раз заметить что `xvdb` как раз таки и отдан полностью под `PV`. Недостатком тут как раз является то, что если используется несколько `VG` - расширение представляется сложным ибо диск отданный под одну группу не может быть занят другой. 
    Такое разбиение было примененно  на дсике `xvdb`.


2. Диск содержит логические (или виртуальные или лвм) разделы без наличия разделов расширения (`extendet partitions`). 

    Тут ситуация аналогчино с диском `xvdc`. Там можно заметить, что был создан диск, далее на нём создан раздел, а внутри данного раздела уже размещаются LVM. Достоинства заключаются в возможности в свободе раздела диска для разых `VG` и `PV`. недостатком являеется то что приходится находить различные хитрости расширения диска в омногом на горячуюю, ибо нельзя просто так взять и остановть обращения к бд для отмонтирования разделов, особенно если их несколкьо и они в lvm. собственно из за данной разметки и сущестсвует данная инструкция. 

3. Диск содержит первичные записи и последующие разделы созданы с помощью расширенного типа раздела в самом начале установки ОС. 

    Это можно видеть на примере `xvda`  `extendet partitions`. особенность расширения при такой разбивке заключается в том что сначала нужно увеличить расширяющий раздел  тролько потом логический. данный метод будет показан на прмере работы с `parted` утилитой.
 


Есть два пути расширения первый через `parted` что крайне прекрасно и второе через `fdisk` что страшно, но мы выберем сначала второй вариант, а затем на другом разделе используем первый.

---
### fdisk way

1. Выбираем наш диск для работы с ним  `fdisk /dev/xvdc`
```sh
root@debian:~# fdisk /dev/xvdc

Welcome to fdisk (util-linux 2.29.2).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): _
```

2. Далее Вводим комменду `p` - для отображения того что у нас есть.
```sh
Command (m for help): p
Disk /dev/xvdc: 4 GiB, 4294967296 bytes, 8388608 sectors # Здесь видно сколько секторов доступно всего
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x2542af6f

Device     Boot Start     End Sectors Size Id Type
/dev/xvdc1       2048 6291015 6288968   3G 83 Linux   # А здесь показано сколько секторов используется
```

3. видим разницу и приступаем к расширению (**ЧИТАТЬ ВНИМАТЕЛЬНО!**):

```sh
Command (m for help): d   # удаляем логический раздел (не беспокоимся все данные будут сохранены ибо удаляем маркировку раздела, а не зануляем диск)
Selected partition 1      # поскольку у нас только один раздел (видно из пункта 2) то его и удоляем
Partition 1 has been deleted.  # удалено

Command (m for help): n   # создаём новый раздел
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p      # он у нас первичный так что таким и оставим
Partition number (1-4, default 1):                                              # а вот эти
First sector (2048-8388607, default 2048):                                      #    параметры оставляем
Last sector, +sectors or +size{K,M,G,T,P} (2048-8388607, default 8388607):      #           без изменений! fdisk сам умеет неплохо выдеть границы секторов

Created a new partition 1 of type 'Linux' and of size 4 GiB.
Partition №1 contains a LVM2_member signature.   #  здесь должна быть решётка (#) но я заменил на номер (№) для лучшего отображения  здесь 

Do you want to remove the signature? [Y]es/[N]o: n                              # **обязательно** указываем n нам нужно сохранить данные

### ОЧЕНЬ ВАЖНЫЙ МОЕНТ! НИЖЕ КОМАНДА НАЧИНАЕТ ЗАПИСЬ ПЕРЕРАЗМЕТКИ! ###
Command (m for help): w 

The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Device or resource busy

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).
# поскольку раздел мы не отмонтировали то нам и говорится что будет доступно или после перезагрузке или применив partprobe
# используем второй варинт
root@debian:~# partprobe        # может занимать некторое время ибо перечитывает таблицу
```
4. После  можно начинать расширять уже логику с помощью `pvresize` `vgresize` `lvextend`
```sh
root@debian:~# pvs                         # сначала смотрим что есть
  PV         VG             Fmt  Attr PSize PFree
  /dev/xvda5 debian-vg      lvm2 a--  7.76g    0 
  /dev/xvdb  debian-vg-home lvm2 a--  8.00g 4.00g
  /dev/xvdc1 debian-vg      lvm2 a--  3.00g    0 

root@debian:~# pvresize /dev/xvdc1         # ресайзим 

  Physical volume "/dev/xvdc1" changed
  1 physical volume(s) resized / 0 physical volume(s) not resized
root@debian:~/# pvs                         # и получаем свободное пространство
  PV         VG             Fmt  Attr PSize PFree
  /dev/xvda5 debian-vg      lvm2 a--  7.76g    0 
  /dev/xvdb  debian-vg-home lvm2 a--  8.00g 4.00g
  /dev/xvdc1 debian-vg      lvm2 a--  4.00g 1.00g  # от он наш гигабайт
```

5. Проверим видимую облась для `Volume Group (vg)`  увеличим обьём 

```sh
root@debian:~# vgs
  VG             #PV #LV #SN Attr   VSize  VFree
  debian-vg        2   3   0 wz--n- 11.76g 1.00g    # видим что есть свободное место для группы debian-vg
  debian-vg-home   1   1   0 wz--n-  8.00g 4.00g
```

6. Ну и далее просто увеличим раздел на половину от свободного места и расширим файловую систему.
```sh

root@debian:~# lvextend /dev/debian-vg/root -l +50%FREE              # добавляем половину места от доступного места к разделу / 
  Size of logical volume debian-vg/root changed from 8.51 GiB (2179 extents) to 10.00 GiB (2559 extents).
  Logical volume debian-vg/root successfully resized.  # сообщение об успешном увеличении

 # далее расширяем файловую систему, что бы система видела свободное место доступное для записи 
root@debian:~# resize2fs /dev/debian-vg/root 
resize2fs 1.43.4 (31-Jan-2017)
Filesystem at /dev/debian-vg/root is mounted on /; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 2
The filesystem on /dev/debian-vg/root is now 2620416 (4k) blocks long.  # сообщение об увеличении

root@debian:~# df -h
Filesystem                                 Size  Used Avail Use% Mounted on
udev                                       277M     0  277M   0% /dev
tmpfs                                       58M  2.7M   56M   5% /run
/dev/mapper/debian--vg-root                9.8G  1.6G  7.8G  17% /              # собственно видим увеличенное место
tmpfs                                      289M     0  289M   0% /dev/shm
tmpfs                                      5.0M     0  5.0M   0% /run/lock
tmpfs                                      289M     0  289M   0% /sys/fs/cgroup
/dev/mapper/debian--vg--home-vg----LVhome  4.0G  4.1M  3.8G   1% /home
/dev/xvda1                                 236M   63M  161M  28% /boot
tmpfs                                       58M     0   58M   0% /run/user/1000

# или вот так
root@debian:~# lvs
  LV         VG             Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup     debian-vg      -wi-a-----   2.53g                                                    
  root       debian-vg      -wi-ao----  10.00g                                                    
  swap_1     debian-vg      -wi-ao---- 728.00m                                                    
  vg--LVhome debian-vg-home -wi-ao----   4.00g                                                    

```

---
### parted way

 На самом деле самый приятный метод ибо размечает за тебя (почти) и шанс накосячить - меньше.
 Недостаток лишь в том, что расширитья, как я думаю можно только последний диск а по середине там ничего толком не сделаешь, что опять же толкает нас на использование тотального LVM.

Поскольку мы работаем с разделом который был настроен при установке и на нём уже есть первичная запись в виде  `/boot` (xvda1)  о тут всё идёт немного по другому. вообще здесь при установке используется таблица разделов `MBR` а не `GPT` то для начала расширим `xvda2` ибо он является расширяющим разделом для `xvda5`, а потом непосредственно и сам `xvda5`.

 Для начала выбираем диск  приступаем к работе:
```sh
    root@debian:/home/user# parted /dev/xvda
    GNU Parted 3.2
    Using /dev/xvda
    Welcome to GNU Parted! Type 'help' to view a list of commands.

    (parted) p         # обязательно вводим данную команду (print) для получения информации                                                       
    Model: Xen Virtual Block Device (xvd)
    Disk /dev/xvda: 10.7GB                  # здесь отображается доступное место 
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags: 

    Number  Start   End     Size    Type      File system  Flags
     1      1049kB  256MB   255MB   primary   ext2
     2      257MB   8589MB  8332MB  extended                        # видим что есть различия с доступным местом
     5      257MB   8589MB  8332MB  logical                lvm      

    (parted) resizepart 2  # команда расширения, которая указывает на раздел расширения (Type - extended)
    End?  [8589MB]? 10.7GB   # Указываем размер полученный выше      

```
 
 Проверяем полученный результат:

```sh
  (parted) p         # обязательно вводим данную команду (print) для получения информации                                                       
    Model: Xen Virtual Block Device (xvd)
    Disk /dev/xvda: 10.7GB                  # здесь отображается доступное место 
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags: 

    Number  Start   End     Size    Type      File system  Flags
     1      1049kB  256MB   255MB   primary   ext2
     2      257MB   10.7GB  10.4GB  extended                        # видим что место расширилось
     5      257MB   8589MB  8332MB  logical                lvm      # теперь приступаем к расширению раздела где у нас находятся lvm

    (parted) resizepart 5  # теперь вуаля расширяем раздел 5
    End?  [8589MB]? 10.7GB   # Указываем размер полученный выше      

    (parted) p          # проверяем      
    Model: Xen Virtual Block Device (xvd)
    Disk /dev/xvda: 10.7GB               
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags: 

    Number  Start   End     Size    Type      File system  Flags
     1      1049kB  256MB   255MB   primary   ext2
     2      257MB   10.7GB  10.4GB  extended                        
     5      257MB   10.7GB  10.4GB  logical                lvm      # it's work!
    
    (parted) quit         # выходим      
         
    Information: You may need to update /etc/fstab. # Возможно требуется перечитка  fstab-а, а может и нет =)
             (volume group) (device pv)
    vgextend       vg       /dev/sdc1
```
 А дальше всё делается как в инструкции выше, начиная с пункта 4.
 
 
