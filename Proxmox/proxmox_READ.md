# Proxmox рецепты и особенности

Данная система использует два вида виртуализации контейнер и ВМ.
Хранилище для ВМ и контейнеров используют LVM.
Для управления виртуальными машинами использут 
`qm - Qemu/KVM Virtual Machine Manager`

примеры команд:
```sh
 qm importdisk VMID your_image.img STORAGE-ID  #импортирует диск в хранилище и добавляет к указанной ВМ

 qm list # показывает ИД всех машин
```
Более полное описание находится здесь: https://pve.proxmox.com/pve-docs/qm.1.html

Для управлением хранилищем используют `pvesm - Proxmox VE storage manager`
многие команды для данного менеджера используют Storage_ID его можно посмотреть в файле  `/etc/pve/storage.cfg`. Как показывает практика ID это **название**, так что не ищи набор рандомных символов.

```sh
<TYPE>  <Storage_ID>
lvmthin: local-lvm
	thinpool data
	vgname pve
	content images,rootdir
```

примеры комманд
```sh
- pvesm list local-lvm # показывает какие диски находятся в хранилище local-lvm

Volid                   Format  Type             Size VMID
local-lvm:vm-100-disk-0 raw     images    34359738368 100
local-lvm:vm-102-disk-0 raw     images     9663676416 102
local-lvm:vm-102-disk-1 raw     images     8589934592 102

```
Более полное описание находится здесь: https://pve.proxmox.com/pve-docs/pvesm.1.html

Один из вариантов переноса данных:
`$ dd if=SVM.image_KVM_4.0.41.8018.raw bs=1M | ssh <destination ip> dd of=/dev/<vgname>/vm-<vmid>-disk-1 bs=1M` - используется от хоста к хосту