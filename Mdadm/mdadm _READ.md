# mdadm - создание софтверных рейдов. 

more info on [this site](http://xgu.ru/wiki/mdadm#mdadm)

Зануляем суперблоки на sdb sdc устройствах (будем создавать RAID1)
 `mdadm --zero-superblock --force /dev/sd{b,c}`

При выводе :
```sh
mdadm: Unrecognised md component device - /dev/sdb
mdadm: Unrecognised md component device - /dev/sdc
```
Означает что диски не были задействованы в ранее в рейде и не является ошибкой.

Далее зачистим метаданные (магические строки, различные подписи и пр.) с дисков.
`wipefs --all --force /dev/sd{b,c}`

Далее создадим рэйд массив :
`mdadm --create --verbose /dev/md127 -l 1  -n 2 /dev/sd{b,c}`

`/dev/md127` - устройство которое будет итогом обьединения двух дисков.
`-l 1` или `--level=1` - указывает тип рейда В данном случае RAID 1.
`-n 2` или `--raid-devices=2` - указываем количество устройств отданных под рейд.
`/dev/sd{b,c}` - перечесление устройств входящих в рейд.

В дальнейшем нас попросит подтвердить создание массива и мы это делаем нажав `y`.
`Continue creating array? y`

Проверить конфигурацию можно выполнив:
`cat /proc/mdstat`
Примерный вывод:
```sh
md127 : active raid1 sdb[0] sdc[1]
      1953382464 blocks super 1.2 [2/2] [UU]  # обрати внимание на UU значит что все диски находятся в использовании
      bitmap: 0/15 pages [0KB], 65536KB chunk

unused devices: <none>
```

Далее нам необходимо сохранить параметры данного рейда в конфиг файле:
`echo "DEVICE partitions" > /etc/mdadm/mdadm.conf`
`mdadm --detail --scan --verbose | awk '/ARRAY/ {print}' >> /etc/mdadm/mdadm.conf`

Ну и не забудем сделать автоматическое примонтирование в `/etc/fstab`:
```sh
# в данном случе этот линк ведет на /dev/md127 так что ничего сверхестественного
/dev/md/by05:0 /mnt/backup ext4 defaults 0 0
```