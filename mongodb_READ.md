# MONGODB - Краткое описание

Подключаться необходимо с хоста, ибо по умолчанию слушает 127.0.0.1
сервис (демон) отвечающий за mongo - `mongod`
```sh
# проверка статуса сервиса
systemctl status mongod.service
service mongod status
```
Подключение происходит через команду `mongo`
```sh
#      -u (set user) -p (password)    db with valid user account
mongo -upraca_mongo -pTrV6vWkb5hYz --authenticationDatabase admin
```
### Бэкап БД
---
```sh
# утилита                                                               #название БД   #выходная директория
mongodump -upraca_mongo -pTrV6vWkb5hYz --authenticationDatabase admin  --db praca --out ./`date +"%d-%m-%y"`  
```

Можно также применить сжатие. И зачастую даже нужно, не так быстро получается бэкап, заато умешьние данных идёт с 60 Гб до 2 Гб, что весьма актуально.
```sh
                                                                                                            # применить сжатие
mongodump -upraca_mongo -pTrV6vWkb5hYz --authenticationDatabase admin  --db praca --out ./`date +"%d-%m-%y"`  --gzip
```

### Восстановление БД
---
Восстановление: (и помним про все слеши и пробелы)
```sh
mongorestore -upraca_mongo -pTrV6vWkb5hYz --authenticationDatabase admin  --db praca --drop /<our_date>/<NameOfDB>/
```
и 
```sh
# Если было всё заархивировано то используем строку ниже
mongorestore -upraca_mongo -pTrV6vWkb5hYz --authenticationDatabase admin  --db praca --drop /<our_date>/<NameOfDB>/ --gzip 
```
