# Небольшие заметки по ксен серверу, должны быть полезны начинающему.

Неплохая справочная [информация по командам](https://linuxconfig.org/xe-full-command-list-reference-with-description-for-xenserver).

Включение автозапуска для виртуальных машин на ксене:

1. Включит автозапуск для пула `xe pool-param-set other-config:auto_poweron=true uuid=<pool_UUID>`
2. А затем индивидуально для каждой машины `xe vm-param-set other-config:auto_poweron=true uuid=<VM UUID>`

Миргрировать виртуальный машины лучше от процессора к процесорру (intel to Intel, AMD to AMD). Посмотреть можно:

   ```sh
    xe host-cpu-info
   ```

Показать список дисков  

   ```sh
    xe vdi-list 
   ```

---
При установки из XenCenter выбирается предполагаемая модель дистрибутива, я хз как это

**/var/opt/ISO_IMAGES** - папка куда можно добавлять ISO образы для дальнейшего использования при разворачивании. **Работает только с XenWeb**

Добовлять образа можно сколь угодным способом, но я предпочитаю через `wget`.

```sh
wget http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.9.0-amd64-netinst.iso
# и не забываем скачивается файл по вашему текущему расположению
```
