# Ошибки XenServer-a

### SR_BACKEND_FAILURE_109

---

```sh
Error code: SR_BACKEND_FAILURE_109
Error parameters: , The snapshot chain is too long, 
```

Ознакомится с рекомендациями и некоторыми исправлениями можно [тут](https://support.citrix.com/article/CTX201296) - https://support.citrix.com/article/CTX201296

Поскольку у нас тип дииски созданы с помошью `lvm` то:

```sh
#сначала посмотрим на спиок устройств 
lsblk
#если ниже не понятно - нагугли  # указываем uuid SR  затем uuid VHD диска нашей машинки 
vhd-util  query  -vsfd  -p  -n    /dev/VG_XenStorage-20bfc59f-604a-cbe8-19a9-3492c5c3895d/VHD-ca8fdfea-640a-44a6-b5f0-dfe33f92a93b
```

Вывод будет приблизительно таким:

```sh
512000
189864690176
/dev/mapper/VG_XenStorage--20bfc59f--604a--cbe8--19a9--3492c5c3895d-VHD--95a8e5fc--68ed--4c03--8eb0--2bd8c6a5354a
hidden: 0
chain depth: 30 # данное число означает что мы достигли предела в количестве снимков
```

Далее нужно зпустить garbage collector (gc) которые подчистит мусор и склеит снимки (coalesing)

```sh
xe sr-scan uuid=<your_SR_uuid> 
```

Может показаться что команда отработала, но на самом деле это процесс довольно долгий и за "ходом" можно следить смотря в файл 

`/var/log/SMlog`

Неплохая [статья](https://wysotsky.info/ru/xenserver-%D0%B8-%D0%BE%D1%88%D0%B8%D0%B1%D0%BA%D0%B0-sr_backend_failure_109/) по данному случаю.

### SR_BACKEND_FAILURE_44

---

В процессе создания снимка может появится ошибка типа :
  
```sh
  Error code: SR_BACKEND_FAILURE_44
  Error parameters: , There is insufficient space, 
```
  
Это говорить что SR - (storage repository) где делается первоначальный снэпшот просто напросто заполнен и не может создать снэпшот.
  
Что может помочь:

- удаление какой нить виртуалки с её диском для освобождения SR и запуск команды `xe sr-scan uuid=<SR-UUID>` поскольку выполнит слияние и очистку ненужного снимка.
- расширить SR (пока не нашёл информации как это реализоват удобно)
- мб переписать скрипт для последовательного создания и удаления вм, а не всё в конце... (но это не точно)

### Сбор логов

---

```sh
xen-bugtool --yestoall          #сбор багов и составление отчёта может выдать огромный список информации и возможно не очень приятных слов, но терпение и черз минут 20 - 30 должен появиться  отчёт
```

Так же можно и нужно смотреть в `/var/log/SMlog` и `/var/log/messages` ну и лишним не будет заглянуть в `journalctl -xe`

### Изменился имя SR хранилища а также uuid PV

---
В офисе как то раз пропало электричество и отьехал XenServer. При заупске не стартанули многие виртуалки, ибо был отключен автозапуск (том числе и виртуалку со всеми креденшелами)
По итогу оказалось что `VG` сменил свой `uuid` за ним и блочное устройство `PV` сменило uuid на новый.

**ОСНОВА ЗАМЕТКИ** - https://support.citrix.com/article/CTX116017
При всех нюансах обращаться к ней.

Но благо это всё в `lvm` по этому этому идём в `/etc/lvm/backup`  видим предыдущее значение `VG`  обязательно копируем всю папку к себе в `~/backup_lvm`.

Начинаем восстановление:

1. Переименуем `volume group` на правильное значение:

```sh
vgrename VG_XenStorage-<incorrect uuid> VG_XenStorage-<correct uuid> --config global{metadata_read_only=0}
```

2. Далее нам нужно получит `uuid` нового `PV`

```sh
pvdisplay

#result 
 --- Physical volume ---
  PV Name               /dev/sdc
  VG Name               VG_XenStorage-5ea24da3-a869-5b41-4951-8f59e93ff874
  PV Size               893,75 GiB / not usable 12,00 MiB
  Allocatable           yes 
  PE Size               4,00 MiB
  Total PE              228797
  Free PE               228796
  Allocated PE          1
  PV UUID               gfrnun-DIik-l7ld-fD41-MuyH-vzt7-zGq0cC
   
  --- Physical volume ---
  PV Name               /dev/sda3
  VG Name               VG_XenStorage-6522e5d4-1abc-763e-0fd5-4c74ebac7b39  # должен уже отобразиться правильный VG
  PV Size               3,63 TiB / not usable 14,98 MiB
  Allocatable           yes 
  PE Size               4,00 MiB
  Total PE              951548
  Free PE               947183
  Allocated PE          4365
  PV UUID               VZc92u-OxfB-ZuCS-TpoJ-w8ka-wocf-8CBkXl  #  вот и наш uuid

```

3. Далее работаем с файлами по пути `~/backup_lvm`. `PV UUID` подстваим в  `pv0` в файле c **правильным** `uuid ( VG_XenStorage-6522e5d4-1abc-763e-0fd5-4c74ebac7b39)` который использовали выше.

Должно получится что то подобное:

```sh
VG_XenStorage-6522e5d4-1abc-763e-0fd5-4c74ebac7b39 {
	id = "4Ouhd6-qLVX-JE9Z-Jn6h-joS7-c4cF-axNECp"
	seqno = 122
	format = "lvm2"			# informational
	status = ["RESIZEABLE", "READ", "WRITE"]
	flags = []
	extent_size = 8192		# 4 Megabytes
	max_lv = 0
	max_pv = 0
	metadata_copies = 0

	physical_volumes {

		pv0 {
			id = "аf5n6n-DIik-l7ld-fD41-MuyH-vzt7-zGq0cC"   #вот строчка которую нужно заменить на правильную
     #вот на это "VZc92u-OxfB-ZuCS-TpoJ-w8ka-wocf-8CBkXl"
			device = "/dev/sdc"	# Hint only

			status = ["ALLOCATABLE"]
			flags = []
			dev_size = 1874329600	# 893.75 Gigabytes
			pe_start = 22528
			pe_count = 228797	# 893.738 Gigabytes
		}
	}
    ... # next lines
```

4. Начнём восстановление нашего волюм груп (сначала в тестовом режиме)

```sh
vgcfgrestore --test --file  /root/backup_lvm/VG_XenStorage-<sr uuid> VG_XenStorage-<sr uuid> --config global{metadata_read_only=0}
```

5. Если всё произошло без ошибок, то запускаем на живую.

```sh
vgcfgrestore --file  /root/backup_lvm/VG_XenStorage-<sr uuid> VG_XenStorage-<sr uuid> --config global{metadata_read_only=0}
```

На этом всё, а дальше нужно возится с подключением `xe pbd-plug/unplug` ну а это уже добро пожаловать в документацию.
