# Немного об SSL Сертефикатах

Просто полезная [статья](https://github.com/MaksimDzhangirov/complete-gRPC/blob/main/SSL_TLS_lecture_rus.md)

Если не заниматься самоподписными сертификатами потипу `Let's Encrypt`, то покупаются и используется основные типы сертификатов:

1. **Обычные SSL-сертификаты** - выпускаются автоматически. Основное преимущество — они выпускаются очень быстро. Обратная сторона — они очень простые, и подойдут только для тех сайтов, где нет необходимости в доверии со стороны пользователей.

2. **Wildcard-сертификаты** - предназанчены для обеспечения безопасности не только *основного домена*, но и *поддомены* (forum.your_domain.com, support.ypur_domain.com и т.д.).

3. **SAN-сертификаты** - используются для нескольких доменов, которые размещены на одном сервере.

4. **EV-сертификаты** — сертификаты с максимальной степенью защиты, которые полностью подтверждают существование компании, осбенность заключается в том что при проведении покупок или других ценных операций пользователь знает что этому сайту можно доверять и в случае чего потребовать компенсацию.

Для выпуска сертификата нужно будет сгененрировать и предоставить приватный ключ, ибо на основе него генерируется основной сертификат.

Вообще сам сертификат может состоять из нескольких отдельных файлов или сразу будет обьединён в один большой.
Например при заказе сертификата `wildcard` центр сертификации прислал четыре файла:

- **AAACertificateServices.crt** - корневой сертификат центра сертификации.
- **USERTrustRSAAAACA.crt** - промежуточный сертификат центра сертификации.
- **RSADomainValidationSecureServerCA.crt** - промежуточный сертификат центра сертификации.
- **STAR_*<your_domain>*.crt** - сертификат домена (PositiveSSL Certificate)

В другом же центре сертификации прислали `wildcard` сразу готовый сертификат формата

- **\*.your_domain.com.pem** - где и было скормлено nginx в таком виде.

## Let's  Encrypt 2021

The reason is that the "DST Root CA X3" certificate has expired yesterday.

To fix it, just disable the certificate on your server. Run:

sudo dpkg-reconfigure ca-certificates
On the first screen that prompts "Trust new certificates from certificate authorities?" choose "yes". On the next screen press the down arrow key on your keyboard until you find mozilla/DST_Root_CA_X3.crt, press the space bar to deselect it (the [*] should turn into [ ]) and press Enter.

https://superuser.com/questions/1679204/curl-on-ubuntu-14-all-lets-encrypt-certificates-are-expired-error-60